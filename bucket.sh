#!/bin/bash
#author: wilfried Tchako
#creation date : 07/06/2021
#last modified : 10/20/2022
# description : this script will create a  s3 bucket to backup my jenkins server 
# usage : You will run this script withnin a jenkins pipeline
S3_BUCKET=random_name


if aws s3 ls "s3://$S3_BUCKET" 2>&1 | grep -q 'NoSuchBucket'
then
    echo "$S3_BUCKET doesn\'t exist please check again"
    aws cloudformation create-stack --stack-name random_name --template-body file://s3.yaml
else
    echo " the $S3_BUCKET already exist"
fi

